/* Tri Graphique - Environnement de visualisation graphique d'algorithmes de tri
 * Copyright (C) 2018 Guillaume Huard
 * 
 * Ce programme est libre, vous pouvez le redistribuer et/ou le
 * modifier selon les termes de la Licence Publique Générale GNU publiée par la
 * Free Software Foundation (version 2 ou bien toute autre version ultérieure
 * choisie par vous).
 * 
 * Ce programme est distribué car potentiellement utile, mais SANS
 * AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties de
 * commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la
 * Licence Publique Générale GNU pour plus de détails.
 * 
 * Vous devez avoir reçu une copie de la Licence Publique Générale
 * GNU en même temps que ce programme ; si ce n'est pas le cas, écrivez à la Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
 * États-Unis.
 * 
 * Contact:
 *          Guillaume.Huard@imag.fr
 *          Laboratoire LIG
 *          700 avenue centrale
 *          Domaine universitaire
 *          38401 Saint Martin d'Hères
 */

class ES
{
    final static int nbMin = 100;
    final static int nbMax = 100;
    final static int valMin = 0;
    final static int valMax = 49;
    
    public static int [] lireTableau(java.util.Scanner in) {
        System.out.println("Saisissez un entier");
        int [] T = new int[in.nextInt()];
        System.out.println(T.length + " autres entiers");
        for (int i=0; i<T.length; i++)
            T[i] = in.nextInt();
        return T;
    }
    
    public static int [] genererTableau(java.util.Random generateur) {
        int [] T = new int[generateur.nextInt(nbMax-nbMin+1)+nbMin];
        for (int i=0; i<T.length; i++)
            T[i] = generateur.nextInt(valMax-valMin+1)+valMin;
        System.out.println("Tableau généré");
        ES.afficherTableau(System.out, T);
        System.out.println();
        return T;
    }
    
    public static int genererEntier(java.util.Random generateur) {
       int x;
        x = generateur.nextInt(valMax-valMin+1)+valMin;
        System.out.println("Element généré");
        System.out.println(x);
        return x;
    }

    public static void afficherTableau(java.io.PrintStream out, int [] T) {
        out.print(java.util.Arrays.toString(T));
    }
}
