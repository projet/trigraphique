/* Tri Graphique - Environnement de visualisation graphique d'algorithmes de tri
 * Copyright (C) 2018 Guillaume Huard
 * 
 * Ce programme est libre, vous pouvez le redistribuer et/ou le
 * modifier selon les termes de la Licence Publique Générale GNU publiée par la
 * Free Software Foundation (version 2 ou bien toute autre version ultérieure
 * choisie par vous).
 * 
 * Ce programme est distribué car potentiellement utile, mais SANS
 * AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties de
 * commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la
 * Licence Publique Générale GNU pour plus de détails.
 * 
 * Vous devez avoir reçu une copie de la Licence Publique Générale
 * GNU en même temps que ce programme ; si ce n'est pas le cas, écrivez à la Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
 * États-Unis.
 * 
 * Contact:
 *          Guillaume.Huard@imag.fr
 *          Laboratoire LIG
 *          700 avenue centrale
 *          Domaine universitaire
 *          38401 Saint Martin d'Hères
 */

import java.util.Arrays;

public class Validateur {
	private int [] Copie;
	
	public Validateur(int [] T) {
		Copie = Arrays.copyOf(T, T.length);
	}
	public void verifieTri(int [] T) {
		boolean trie = true;
		boolean permutation = true;
		boolean [] marque;
		
		marque = new boolean[T.length];
		for (int i=0; i<T.length; i++) {
			if (i>0) {
				trie &= (T[i-1] <= T[i]);
			}
			int j;
			for (j=0; (j<marque.length) && (marque[j] || (T[i] != Copie[j])); j++) {
			}
			if (j<marque.length) {
				marque[j] = true;
			} else {
				permutation = false;
			}	
		}
		if (!trie) {
			System.err.println("*** BUG : Tableau non trié ***");
		}
		if (!permutation) {
			System.err.println("*** BUG : Elements du tableau changés ***");			
		}
	}
	

}
