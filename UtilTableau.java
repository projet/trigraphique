/* Tri Graphique - Environnement de visualisation graphique d'algorithmes de tri
 * Copyright (C) 2018 Guillaume Huard
 * 
 * Ce programme est libre, vous pouvez le redistribuer et/ou le
 * modifier selon les termes de la Licence Publique Générale GNU publiée par la
 * Free Software Foundation (version 2 ou bien toute autre version ultérieure
 * choisie par vous).
 * 
 * Ce programme est distribué car potentiellement utile, mais SANS
 * AUCUNE GARANTIE, ni explicite ni implicite, y compris les garanties de
 * commercialisation ou d'adaptation dans un but spécifique. Reportez-vous à la
 * Licence Publique Générale GNU pour plus de détails.
 * 
 * Vous devez avoir reçu une copie de la Licence Publique Générale
 * GNU en même temps que ce programme ; si ce n'est pas le cas, écrivez à la Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307,
 * États-Unis.
 * 
 * Contact:
 *          Guillaume.Huard@imag.fr
 *          Laboratoire LIG
 *          700 avenue centrale
 *          Domaine universitaire
 *          38401 Saint Martin d'Hères
 */

import java.util.Random;

class UtilTableau {

    private MachineTrace m;
    int delai;
	private int comparaisons;

    public UtilTableau(int d) {
        m = new MachineTrace(800, 400);
        m.rafraichissementAutomatique(false);
        m.masquerPointeur();
        delai = d;
		RAZComparaisons();
    }

    public void fixerDelai(int d) {
        delai = d;
    }

	public void RAZComparaisons() {
		comparaisons=0;
	}

	public int comparaisons() {
		return comparaisons;
	}

	public int compare(int a, int b) {
		comparaisons++;
		if (a<b) return -1;
		if (a==b) return 0;
		return 1;
	}

    public void afficherLigne(int[] T, int i) {
        int line = Math.min(T[i] * 5, 280);
        m.lever();
        m.placer(i * 4 - T.length * 2, -140);
        m.orienter(90);
        m.baisser();
        m.avancer(line);
    }

    public void afficherTableau(int[] T) {
        m.attendre(delai);
        m.effacerTout();
        for (int i = 0; i < T.length; i++) {
            afficherLigne(T, i);
        }
        m.rafraichir();
    }

    public void echanger(int[] T, int x, int y) {
        if (x != y) {
            int couleur = m.couleurCourante();
            m.attendre(delai);
            m.changeCouleur(MachineTrace.BLANC);
            afficherLigne(T, x);
            afficherLigne(T, y);
            int tmp = T[y];
            T[y] = T[x];
            T[x] = tmp;
            m.changeCouleur(couleur);
            afficherLigne(T, x);
            afficherLigne(T, y);            
            m.rafraichir();
        }
    }

    public static void effectuerUnTri(Tri t) {
        effectuerUnTri(t, new Random(), 100);
    }
    
    public static void effectuerUnTri(Tri t, int graine) {
        effectuerUnTri(t, new Random(graine), 100);
    }
    
    public static void effectuerUnTri(Tri t, int graine, int delai) {
        effectuerUnTri(t, new Random(graine), delai);
    }
    
    public static void effectuerUnTri(Tri t, Random generateur, int delai) {
        UtilTableau util = new UtilTableau(delai);
        int[] T;

        T = ES.genererTableau(generateur);
        Validateur valid = new Validateur(T);

        t.trier(util, T);
        valid.verifieTri(T);

        System.out.println("Tableau trié");
        ES.afficherTableau(System.out, T);
        System.out.println();
        System.out.println("Taille du tableau : " + T.length);
		System.out.println("Comparaisons effectuées : "+util.comparaisons());
    }
}
